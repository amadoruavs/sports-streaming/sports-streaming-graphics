import React from 'react';
import { Indicator } from './Indicator.jsx';
import '../../fonts.css'
import AmadorUAVs from '../../assets/amadoruavs-flat.png';

export function DownIndicator({ down, style, logoRef }) {
    console.log(AmadorUAVs);
    return (
        <div style={{ position: "relative", display: "flex", ...style }}>
            <Indicator
                style={{
                    background: "#FFF",
                    color: 'black',
                    width: 225,
                }}
            >
                { down }
            </Indicator>
            <img
                ref={logoRef}
                src={AmadorUAVs}
                style={{
                    position: 'relative',
                    width: 'auto',
                    height: 77,
                    right: 25,
                    top: -1
                }}
            />
        </div>
    );
}
