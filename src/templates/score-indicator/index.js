import React from 'react';
import { TeamIndicator }  from './TeamIndicator.jsx';
import { GameIndicator }  from './GameIndicator.jsx';
import { DownIndicator }  from './DownIndicator.jsx';
import { States } from 'caspar-graphics';
import '../../fonts.css'
import '../../animations.css'

export default class ScoreIndicator extends React.Component {
  scoreIndicatorRef = React.createRef();
  logoRef = React.createRef();

  static previewData = {
    team1Name: "AMADOR",
    team1Score: 420,
    team1Color: "#7B00A7",
    team2Name: "FOOTHILL",
    team2Score: 69,
    team2Color: "#3C4FFF"
  }

  state = {
    didMount: true,
    state: null
  }

  componentDidMount() {
    // Instantly update component
    this.setState({ didMount: true });
  }

  componentDidUpdate() {
    console.log(this.state.state, this.props.state);
    if (this.state.state === this.props.state) {
      return; // Don't trigger transition if nothing to transition
    }
    console.log(this.props.state);

    if (this.props.state === States.playing) {
      console.log("play timeline");
      this.scoreIndicatorRef.current.classList.add('si-intro');
      this.scoreIndicatorRef.current.classList.remove('si-outro');
      this.logoRef.current.classList.add('logo-intro');
      this.logoRef.current.classList.remove('logo-outro');
      this.setState({ state: States.playing });
    }
  }

  componentWillLeave(onComplete) {
    this.scoreIndicatorRef.current.classList.remove('si-intro');
    this.scoreIndicatorRef.current.classList.add('si-outro');
    this.logoRef.current.classList.remove('logo-intro');
    this.logoRef.current.classList.add('logo-outro');
    setTimeout(() => {
      onComplete();
    }, 1000);
  }

  render() {
    const { team1Name, team1Score, team1Color, team2Name, team2Score, team2Color } = this.props.data;
    let teamIndicators = null;

    teamIndicators = (
      <>
        <TeamIndicator
          name={team1Name}
          score={team1Score}
          color={team1Color}
        />
        <TeamIndicator
          name={team2Name}
          score={team2Score}
          color={team2Color}
        />
      </>
    );

    return (
      <>
        <div
          ref={this.scoreIndicatorRef}
          style={{
            display: "flex",
            position: "absolute",
            bottom: 140,
            left: 40,
            maxWidth: 0,
            overflow: 'hidden'
          }}
        >
          { teamIndicators || '' }
          <GameIndicator
            quarter="1st"
            time="11:42"
          />
          <DownIndicator
            down="3rd & 2"
            logoRef={this.logoRef}
          />
        </div>
      </>
    )
  }
}
