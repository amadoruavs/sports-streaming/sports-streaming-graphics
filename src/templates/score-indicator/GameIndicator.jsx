import React from 'react';
import { Indicator } from './Indicator.jsx';
import '../../fonts.css'

export function GameIndicator({ quarter, time, style }) {
    return (
        <div style={{ position: "relative", display: "flex", ...style }}>
            <Indicator
                style={{
                    background: "#FFF",
                    color: 'black',
                    width: 225,
                }}
            >
                { quarter } { time }
            </Indicator>
            <div
                style={{
                    position: 'relative',
                    left: 227,
                    width: 2,
                    height: 75,
                    background: "#000",
                }}
            />
        </div>
    );
}
