import React from 'react';
import { Indicator } from './Indicator.jsx'
import '../../fonts.css'

export function TeamIndicator({ name, score, color, style }) {
    return (
        <div style={{ position: "relative", display: "flex", ...style }}>
            <Indicator
                style={{
                    background: color,
                    color: 'white',
                    width: 200
                }}
            >
                { name }
            </Indicator>
            <Indicator
                style={{
                    background: color,
                    color: 'white',
                    width: 100,
                    filter: 'brightness(85%)',
                }}
            >
                { score }
            </Indicator>
        </div>
    );
}
