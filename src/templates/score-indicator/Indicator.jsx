import React from 'react';
import '../../fonts.css';

export function Indicator({ children, style }) {
    return (
            <div
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    position: 'relative',
                    width: 200,
                    height: 75,
                    padding: 15,
                    fontSize: 36,
                    background: '#222',
                    fontFamily: 'Share Tech Mono',
                    color: '#FFF',
                    textOverflow: 'ellipsis',
                    overflow: 'hidden',
                    whiteSpace: 'nowrap',
                    ...style
                }}
            >
                { children }
            </div>
    );
}
